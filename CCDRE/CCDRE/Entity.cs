﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCDRE
{
    /// <summary>
    /// The base class that all entities inherit from
    /// </summary>
    abstract class Entity
    {
        protected Texture2D texture;
        protected float moveSpeed;
        protected float rotRate;
        protected float rotation = 0;
        protected Vector2 origin;
        protected Vector2 position;
        protected bool isAlive = true;
        protected bool outOfBounds = false;
        protected Rectangle collisionRect;
        protected Viewport viewport;
        protected Random rand;
        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }
        }
        public bool OutOfBounds
        {
            get
            {
                return outOfBounds;
            }
        }
        public bool IsAlive
        {
            get
            {
                return this.isAlive;
            }
            set
            {
                this.isAlive = value;
            }
        }
        public Rectangle CollisionRect
        {
            get
            {
                return this.collisionRect;
            }
        }
        public Vector2 Position
        {
            get
            {
                return this.position;
            }
        }
        public Vector2 Origin
        {
            get
            {
                return origin;
            }
        }
        protected static readonly float circle = MathHelper.Pi * 2;


        public abstract void Update(GameTime gameTime);

        public abstract void Draw(SpriteBatch spriteBatch);
    }
}
