﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCDRE
{
    class HUD
    {
        private readonly Texture2D hudTex;
        private readonly SpriteFont font;
        private int playerScore;
        private int playerLives;
        private readonly Viewport viewport;
        private Vector2 hudPosition;
        private Vector2 scoreTextPosition;
        private Vector2 livesTextPosition;
        private const int chocWidth = 32;
        private const int scoreLeftPad = 8;
        private const int livesLeftPad = 24;
        private const int heartsLeftPad = 8;
        private const string scoreString = "Score: ";
        private const string livesString = "Lives: ";
        private readonly IList<Texture2D> heartTextures;
        private readonly Random rand;

        public HUD(Texture2D hudTex, SpriteFont font, Viewport viewport, IList<Texture2D> heartTextures, Random rand)
        {
            this.hudTex = hudTex;
            this.font = font;
            this.viewport = viewport;
            this.heartTextures = heartTextures;
            this.rand = rand;
            hudPosition = Vector2.Zero;
            scoreTextPosition = new Vector2(hudPosition.X + chocWidth + scoreLeftPad, (hudTex.Height / 2) - font.MeasureString(scoreString).Y / 2);
            livesTextPosition = new Vector2(hudTex.Width / 2 + livesLeftPad, (hudTex.Height / 2) - font.MeasureString(livesString).Y / 2);
        }

        public void Update(GameTime gameTime, int playerScore, int playerLives)
        {
            this.playerScore = playerScore;
            this.playerLives = playerLives;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(hudTex, hudPosition, Color.White);
            spriteBatch.DrawString(font, scoreString + playerScore, scoreTextPosition, Color.Violet);
            spriteBatch.DrawString(font, livesString, livesTextPosition, Color.Violet);
            for (int i = 0; i < playerLives; i++)
            {
                spriteBatch.Draw(heartTextures[rand.Next(0, heartTextures.Count)],
                    new Vector2(livesTextPosition.X + font.MeasureString(livesString).X + (heartsLeftPad * i) + (heartTextures[0].Width * i), hudTex.Height / 2 - heartTextures[0].Height / 2), Color.White);
            }
        }
    }
}
