﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCDRE
{
    /// <summary>
    /// In charge of creating, updating and drawing all entities, along with passing through the relevant information
    /// </summary>
    class EntityManager
    {
        private readonly IList<Texture2D> beanTextures;
        private readonly IList<Texture2D> heartTextures;
        private readonly IList<Texture2D> blueExplosion;
        private readonly IList<Texture2D> greenExplosion;
        private readonly IList<Texture2D> pinkExplosion;
        private readonly IList<Texture2D> redExplosion;
        private readonly Texture2D ceaseTexture;
        private const float defaultCeaseSpawnTime = 500;
        private float ceaseSpawnTime;
        private float ceaseTimer;
        private const float beanSpawnTime = 300f;
        private float beanTimer = 0f;
        private readonly List<Bean> beans = new List<Bean>();
        private readonly List<Explosion> explosions = new List<Explosion>();
        private readonly List<Cease> ceases = new List<Cease>();
        private readonly Viewport viewport;
        private KeyboardState keyboardState;
        private readonly Random rand;
        private HUD hud;
        private readonly Player player;
        private readonly SoundEffect pop;
        private bool isPopPlaying = false;
        public int PlayerLives
        {
            get
            {
                return player.Lives;
            }
        }
        public int PlayerScore
        {
            get
            {
                return player.Score;
            }
        }
        private enum Colours
        {
            blue = 0,
            green,
            orange,
            pink,
            purple,
            red,
            white,
            yellow
        };
        // TODO: This method is going to get ugly fast. Need to find a better way of handling all the variable passing
        public EntityManager(Viewport viewport, IList<Texture2D> beanTextures, IList<Texture2D> blueExplosion,
            IList<Texture2D> greenExplosion, IList<Texture2D> pinkExplosion, IList<Texture2D> redExplosion, IList<Texture2D> heartTextures, 
            Texture2D hudTex, Texture2D ceaseTexture, Texture2D playerTex, Random rand, SpriteFont font, SoundEffect pop)
        {
            this.beanTextures = beanTextures;
            this.heartTextures = heartTextures;
            this.blueExplosion = blueExplosion;
            this.greenExplosion = greenExplosion;
            this.pinkExplosion = pinkExplosion;
            this.redExplosion = redExplosion;
            this.ceaseTexture = ceaseTexture;
            this.viewport = viewport;
            this.rand = rand;
            this.pop = pop;
            ceaseSpawnTime = defaultCeaseSpawnTime;
            player = new Player(viewport, playerTex, hudTex.Height);
            hud = new HUD(hudTex, font, viewport, heartTextures, rand);
        }

        public void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            this.keyboardState = keyboardState;
            HandleCollision();
            #region Bean Update
            if (beanTimer >= beanSpawnTime)
            {
                Bean bean = new Bean(viewport, beanTextures[rand.Next(0, Enum.GetNames(typeof(Colours)).Length)], rand);
                beans.Add(bean);
                beanTimer = 0;
            }
            foreach (Bean bean in beans)
            {
                bean.Update(gameTime);
                if (!bean.IsAlive && !bean.OutOfBounds)
                {
                    Explosion explosion = new Explosion(4, 30, redExplosion, bean.Position - bean.Origin);
                    explosions.Add(explosion);
                    if (!isPopPlaying)
                    {
                        pop.Play();
                        isPopPlaying = true;
                    }
                }
            }
            beans.RemoveAll(x => !x.IsAlive);
            beanTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
            #endregion
            #region Cease Update
            if (ceaseTimer >= ceaseSpawnTime)
            {
                Cease cease = new Cease(viewport, ceaseTexture, rand);
                ceases.Add(cease);
                ceaseTimer = 0;
            }
            foreach (Cease cease in ceases)
            {
                cease.Update(gameTime);
                if (!cease.IsAlive && !cease.OutOfBounds)
                {
                    Explosion explosion = new Explosion(4, 40, blueExplosion, cease.Position - cease.Origin);
                    explosions.Add(explosion);
                    //pop.Play();
                }
            }
            ceases.RemoveAll(x => !x.IsAlive);
            ceaseTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
            #endregion
            #region Explosion Update
            foreach (Explosion explosion in explosions)
            {
                explosion.Update(gameTime);
            }
            explosions.RemoveAll(x => x.IsAlive);
            #endregion
            player.Update(gameTime, keyboardState);
            hud.Update(gameTime, player.Score, player.Lives);
            isPopPlaying = false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Bean bean in beans)
            {
                bean.Draw(spriteBatch);
            }

            foreach (Cease cease in ceases)
            {
                cease.Draw(spriteBatch);
            }

            player.Draw(spriteBatch);

            foreach (Explosion explosion in explosions)
            {
                explosion.Draw(spriteBatch);
            }

            hud.Draw(spriteBatch);
        }

        public void HandleCollision()
        {
            // Bean-Player collision
            foreach (Bean bean in beans)
            {
                if (bean.CollisionRect.Intersects(player.CollisionRect) && PixelCollision(bean.CollisionRect, bean.Texture, player.CollisionRect, player.Texture))
                {
                    bean.IsAlive = false;
                    player.Score += bean.Points;
                    if (player.Score % 1000 == 0)
                    {
                        ceaseSpawnTime -= 25f;
                    }
                    if (player.Score % 10000 == 0)
                    {
                        player.Lives++;
                    }
                }
            }

            // Cease-Player collision
            foreach (Cease cease in ceases)
            {
                if (cease.CollisionRect.Intersects(player.CollisionRect) && PixelCollision(cease.CollisionRect, cease.Texture, player.CollisionRect, player.Texture))
                {
                    PlayerDeath();
                }
            }
        }

        public void PlayerDeath()
        {
            foreach (Bean bean in beans)
            {
                bean.IsAlive = false;
            }

            foreach (Cease cease in ceases)
            {
                cease.IsAlive = false;
            }

            player.ResetPlayer();
        }

        public void ResetGame()
        {
            player.Lives = 3;
            player.Score = 0;
            explosions.Clear();
            ceaseSpawnTime = defaultCeaseSpawnTime;
        }

        public bool PixelCollision(Rectangle collisionRect1, Texture2D texture1, Rectangle collisionRect2, Texture2D texture2)
        {
            Color[] dataA = new Color[texture1.Width * texture1.Height];
            texture1.GetData(dataA);

            Color[] dataB = new Color[texture2.Width * texture2.Height];
            texture2.GetData(dataB);

            int top = Math.Max(collisionRect1.Top, collisionRect2.Top);
            int bottom = Math.Min(collisionRect1.Bottom, collisionRect2.Bottom);
            int left = Math.Max(collisionRect1.Left, collisionRect2.Left);
            int right = Math.Min(collisionRect1.Right, collisionRect2.Right);

            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    Color colorA = dataA[(x - collisionRect1.Left) + (y - collisionRect1.Top) * collisionRect1.Width];
                    Color colorB = dataB[(x - collisionRect2.Left) + (y - collisionRect2.Top) * collisionRect2.Width];
                    if (colorA.A > 30 && colorB.A > 30)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
