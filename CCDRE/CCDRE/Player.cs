﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCDRE
{
    class Player : Entity
    {
        private int score;
        private int lives;
        private int bottomPadding = 8;
        private Vector2 spawnPosition;
        private int hudHeight;
        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
            }
        }
        public int Lives
        {
            get
            {
                return lives;
            }
            set
            {
                lives = value;
            }
        }

        public Player(Viewport viewport, Texture2D playerTex, int hudHeight)
        {
            lives = 3;
            score = 0;
            texture = playerTex;
            this.viewport = viewport;
            this.hudHeight = hudHeight;
            spawnPosition = new Vector2(viewport.Width / 2 - playerTex.Width / 2, viewport.Height - playerTex.Height - bottomPadding);
            position = spawnPosition;
            origin = new Vector2(texture.Width / 2, texture.Height / 2);
            rotRate = .25f;
            moveSpeed = 4.0f;
        }

        // HACK: Probably the worst piece of code in this entire game
        public override void Update(GameTime gameTime)
        {
            throw new NotImplementedException();
        }

        public void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            rotation += rotRate;
            rotation = rotation % circle;

            if (keyboardState.IsKeyDown(Keys.A) || keyboardState.IsKeyDown(Keys.Left))
            {
                position.X -= moveSpeed;
            }
            if (keyboardState.IsKeyDown(Keys.D) || keyboardState.IsKeyDown(Keys.Right))
            {
                position.X += moveSpeed;
            }
            if (keyboardState.IsKeyDown(Keys.W) || keyboardState.IsKeyDown(Keys.Up))
            {
                position.Y -= moveSpeed;
            }
            if (keyboardState.IsKeyDown(Keys.S) || keyboardState.IsKeyDown(Keys.Down))
            {
                position.Y += moveSpeed;
            }
            position = Vector2.Clamp(position, new Vector2(0, hudHeight), new Vector2(viewport.Width, viewport.Height));
            collisionRect = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, null, Color.White, rotation, origin, 1f, SpriteEffects.None, 0f);
        }

        public void ResetPlayer()
        {
            position = spawnPosition;
            lives--;
        }
    }
}
