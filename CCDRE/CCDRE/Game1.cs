using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CCDRE
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        Viewport viewport;
        EntityManager entityManager;
        Random rand = new Random();
        KeyboardState keyboardState;
        List<Texture2D> beanTextures = new List<Texture2D>();
        List<Texture2D> heartTextures = new List<Texture2D>();
        List<Texture2D> blueExplosion = new List<Texture2D>();
        List<Texture2D> greenExplosion = new List<Texture2D>();
        List<Texture2D> pinkExplosion = new List<Texture2D>();
        List<Texture2D> redExplosion = new List<Texture2D>();
        Texture2D ceaseAndDesist;
        Texture2D hudTex;
        Texture2D playerTex;
        Texture2D titleScreen;
        SoundEffect pop;
        readonly string pauseText = "PAUSED";
        readonly string gameOverText1 = "Game Over! Your score is: ";
        readonly string gameOverText2 = "Press 'R' to retry or 'Esc' to quit.";
        bool isPauseKeyDown;
        bool pauseKeyDownPrevious;
        enum BeanColours
        {
            blue = 0,
            green,
            orange,
            pink,
            purple,
            red,
            white,
            yellow
        };
        readonly int explosionFrames = 5;
        enum ExplosionColours
        {
            blue = 0,
            green,
            pink,
            red
        };
        enum HeartColours
        {
            blue = 0,
            green,
            orange,
            teal,
            purple,
            red,
            white,
            yellow
        };
        enum GameState
        {
            Main = 0,
            Playing,
            Paused,
            GameOver
        };

        GameState currentGameState;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            viewport = GraphicsDevice.Viewport;
            Window.Title = "Candy Crushing Dodger Rainbow Extreme";
            currentGameState = GameState.Main;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            for (int i = 0; i < Enum.GetNames(typeof(BeanColours)).Length; i++)
            {
                Texture2D texture = Content.Load<Texture2D>("images/beans/bean_" + (BeanColours)i);
                beanTextures.Add(texture);
            }
            for (int i = 0; i < Enum.GetNames(typeof(HeartColours)).Length; i++)
            {
                Texture2D texture = Content.Load<Texture2D>("images/hearts/heart_" + (HeartColours)i);
                heartTextures.Add(texture);
            }
            for (int i = 1; i <= explosionFrames; i++)
            {
                Texture2D texture = Content.Load<Texture2D>("images/explosions/blue/explosionblue0" + i);
                blueExplosion.Add(texture);
                texture = Content.Load<Texture2D>("images/explosions/green/explosiongreen0" + i);
                greenExplosion.Add(texture);
                texture = Content.Load<Texture2D>("images/explosions/pink/explosionpink0" + i);
                pinkExplosion.Add(texture);
                texture = Content.Load<Texture2D>("images/explosions/red/explosionred0" + i);
                redExplosion.Add(texture);
            }
            ceaseAndDesist = Content.Load<Texture2D>("images/candd");
            hudTex = Content.Load<Texture2D>("images/hud/thin_hud");
            font = Content.Load<SpriteFont>("fonts/Kootenay");
            playerTex = Content.Load<Texture2D>("images/lollipop_rainbow");
            titleScreen = Content.Load<Texture2D>("images/title");
            pop = Content.Load<SoundEffect>("sounds/two_tone_nav");
            entityManager = new EntityManager(viewport, beanTextures, blueExplosion, greenExplosion, pinkExplosion, redExplosion, heartTextures, hudTex, ceaseAndDesist, playerTex, rand, font, pop);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            keyboardState = Keyboard.GetState();
            isPauseKeyDown = (keyboardState.IsKeyDown(Keys.P) || keyboardState.IsKeyDown(Keys.Escape));

            // TODO: Add your update logic here
            if (currentGameState == GameState.Main)
            {
                if (keyboardState.IsKeyDown(Keys.Enter))
                {
                    currentGameState = GameState.Playing;
                }
            }
            if (currentGameState == GameState.Playing)
            {
                if (pauseKeyDownPrevious && !isPauseKeyDown)
                {
                    currentGameState = GameState.Paused;
                }
                entityManager.Update(gameTime, keyboardState);
                if (entityManager.PlayerLives == 0)
                {
                    currentGameState = GameState.GameOver;
                }
            }
            else if (currentGameState == GameState.Paused)
            {
                if (pauseKeyDownPrevious && !isPauseKeyDown)
                {
                    currentGameState = GameState.Playing;
                }
            }
            if (currentGameState == GameState.GameOver)
            {
                if (keyboardState.IsKeyDown(Keys.R))
                {
                    entityManager.ResetGame();
                    currentGameState = GameState.Playing;
                }
                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    this.Exit();
                }
            }

            pauseKeyDownPrevious = isPauseKeyDown;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (currentGameState == GameState.Playing || currentGameState == GameState.Paused)
            {
                GraphicsDevice.Clear(Color.Thistle);
            }
            else
            {
                GraphicsDevice.Clear(Color.Black);
            }

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            if (currentGameState == GameState.Main)
            {
                spriteBatch.Draw(titleScreen, Vector2.Zero, Color.White);
            }
            if (currentGameState == GameState.Playing || currentGameState == GameState.Paused)
            {
                entityManager.Draw(spriteBatch);
            }
            if (currentGameState == GameState.Paused)
            {
                spriteBatch.DrawString(font, pauseText, new Vector2(viewport.Width / 2 - font.MeasureString(pauseText).X / 2, viewport.Height / 2 - font.MeasureString(pauseText).Y / 2), Color.Black);
            }
            if (currentGameState == GameState.GameOver)
            {
                spriteBatch.DrawString(font, gameOverText1 + entityManager.PlayerScore, new Vector2(viewport.Width / 2 - font.MeasureString(gameOverText1 + entityManager.PlayerScore).X / 2,
                    viewport.Height / 2 - font.MeasureString(gameOverText1 + entityManager.PlayerScore).Y / 2), Color.Violet);
                spriteBatch.DrawString(font, gameOverText2, new Vector2(viewport.Width / 2 - font.MeasureString(gameOverText2).X / 2,
                    viewport.Height / 2 - (font.MeasureString(gameOverText2).Y / 2 - font.MeasureString(gameOverText1).Y)), Color.Violet);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
