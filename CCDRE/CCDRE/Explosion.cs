﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCDRE
{
    class Explosion : Entity
    {
        private readonly IList<Texture2D> explosionTextures;
        private int currentFrame = 0;
        private readonly int totalFrames;
        private readonly float frameTime;
        private float currentTime;

        public Explosion(int totalFrames, int frameTime, IList<Texture2D> explosionTextures, Vector2 position)
        {
            this.totalFrames = totalFrames;
            this.frameTime = frameTime;
            this.explosionTextures = explosionTextures;
            this.position = position;
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (currentTime >= frameTime)
            {
                currentFrame++;
                currentTime = 0f;
                if (currentFrame > totalFrames)
                {
                    currentFrame = 0;
                    isAlive = false;
                }
            }
            currentTime += (float)gameTime.ElapsedGameTime.Milliseconds;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(explosionTextures[currentFrame], position, Color.White);
        }
    }
}
