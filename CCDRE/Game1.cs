using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CCDRE
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        Viewport viewport;
        EntityManager entityManager;
        Random rand = new Random();
        KeyboardState keyboardState;
        List<Texture2D> beanTextures = new List<Texture2D>();
        List<Texture2D> blueExplosion = new List<Texture2D>();
        List<Texture2D> greenExplosion = new List<Texture2D>();
        List<Texture2D> pinkExplosion = new List<Texture2D>();
        List<Texture2D> redExplosion = new List<Texture2D>();
        Texture2D ceaseAndDesist;
        enum BeanColours
        {
            blue = 0,
            green,
            orange,
            pink,
            purple,
            red,
            white,
            yellow
        };
        int explosionFrames = 5;
        enum ExplosionColours
        {
            blue = 0,
            green,
            pink,
            red
        };

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            viewport = GraphicsDevice.Viewport;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            for (int i = 0; i < Enum.GetNames(typeof(BeanColours)).Length; i++)
            {
                Texture2D texture = Content.Load<Texture2D>("images/beans/bean_" + (BeanColours)i);
                beanTextures.Add(texture);
            }
            for (int i = 1; i <= explosionFrames; i++)
            {
                Texture2D texture = Content.Load<Texture2D>("images/explosions/blue/explosionblue0" + i);
                blueExplosion.Add(texture);
                texture = Content.Load<Texture2D>("images/explosions/green/explosiongreen0" + i);
                greenExplosion.Add(texture);
                texture = Content.Load<Texture2D>("images/explosions/pink/explosionpink0" + i);
                pinkExplosion.Add(texture);
                texture = Content.Load<Texture2D>("images/explosions/red/explosionred0" + i);
                redExplosion.Add(texture);
            }
            ceaseAndDesist = Content.Load<Texture2D>("images/candd");
            entityManager = new EntityManager(viewport, beanTextures, blueExplosion, greenExplosion, pinkExplosion, redExplosion, ceaseAndDesist, rand);
            font = Content.Load<SpriteFont>("fonts/Kootenay");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            keyboardState = Keyboard.GetState();

            // TODO: Add your update logic here            
            entityManager.Update(gameTime, keyboardState);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            entityManager.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
