﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCDRE
{
    class HUD
    {
        private Texture2D hudTex;
        private SpriteFont font;
        private int playerScore;
        private int playerLives;
        private Viewport viewport;
        private Vector2 hudPosition;
        private Vector2 textPosition;

        public HUD(Texture2D hudTex, SpriteFont font, Viewport viewport)
        {
            this.hudTex = hudTex;
            this.font = font;
            this.viewport = viewport;
            hudPosition = new Vector2(0, hudTex.Height);
            textPosition = new Vector2(hudPosition.X + 16, hudTex.Height / 2);
        }

        public void Update(GameTime gameTime, int playerScore, int playerLives)
        {
            this.playerScore = playerScore;
            this.playerLives = playerLives;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(hudTex, hudPosition, Color.White);
            spriteBatch.DrawString(font, "Score: " + playerScore, textPosition, Color.White);
        }
    }
}
