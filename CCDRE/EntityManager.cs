﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCDRE
{
    /// <summary>
    /// In charge of creating, updating and drawing all entities, along with passing through the relevant information
    /// </summary>
    class EntityManager
    {
        private IList<Texture2D> beanTextures;
        private IList<Texture2D> blueExplosion;
        private IList<Texture2D> greenExplosion;
        private IList<Texture2D> pinkExplosion;
        private IList<Texture2D> redExplosion;
        private Texture2D ceaseTexture;
        private float ceaseSpawnTime = 60;
        private float ceaseTimer;
        private List<Bean> beans = new List<Bean>();
        private List<Explosion> explosions = new List<Explosion>();
        private List<Cease> ceases = new List<Cease>();
        private float beanSpawnTime = 30f;
        private float beanTimer = 0f;
        private Viewport viewport;
        private KeyboardState keyboardState;
        private Random rand;
        //private bool simulatePlayerDeath = false;
        //private float deathTimer = 5000;
        //private float currentDeathTime = 0;
        private enum Colours
        {
            blue = 0,
            green,
            orange,
            pink,
            purple,
            red,
            white,
            yellow
        };
        // TODO: This method is going to get ugly fast. Need to find a better way of handling all the texture passing
        public EntityManager(Viewport viewport, IList<Texture2D> beanTextures, IList<Texture2D> blueExplosion,
            IList<Texture2D> greenExplosion, IList<Texture2D> pinkExplosion, IList<Texture2D> redExplosion, Texture2D ceaseTexture, Random rand)
        {
            this.beanTextures = beanTextures;
            this.blueExplosion = blueExplosion;
            this.greenExplosion = greenExplosion;
            this.pinkExplosion = pinkExplosion;
            this.redExplosion = redExplosion;
            this.ceaseTexture = ceaseTexture;
            this.viewport = viewport;
            this.rand = rand;
        }

        public void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            this.keyboardState = keyboardState;
            //if (currentDeathTime >= deathTimer)
            //{
            //    simulatePlayerDeath = true;
            //    currentDeathTime = 0;
            //}
            //currentDeathTime += (float)gameTime.ElapsedGameTime.Milliseconds;
            #region Bean Update
            if (beanTimer >= beanSpawnTime)
            {
                Bean bean = new Bean(viewport, beanTextures[rand.Next(0, Enum.GetNames(typeof(Colours)).Length)], rand);
                beans.Add(bean);
                beanTimer = 0;
            }
            foreach (Bean bean in beans)
            {
                bean.Update(gameTime);
                //if (simulatePlayerDeath == true)
                //{
                //    bean.IsAlive = false;
                //    Explosion explosion = new Explosion(4, 30, redExplosion, bean.Position);
                //    explosions.Add(explosion);
                //}
            }
            beans = beans.Where(x => x.IsAlive).ToList();
            beanTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
            #endregion
            #region Cease Update
            if (ceaseTimer >= ceaseSpawnTime)
            {
                Cease cease = new Cease(viewport, ceaseTexture, rand);
                ceases.Add(cease);
                ceaseTimer = 0;
            }
            foreach (Cease cease in ceases)
            {
                cease.Update(gameTime);
                //if (simulatePlayerDeath == true)
                //{
                //    cease.IsAlive = false;
                //    Explosion explosion = new Explosion(4, 30, blueExplosion, cease.Position);
                //    explosions.Add(explosion);
                //}
            }
            ceases = ceases.Where(x => x.IsAlive).ToList();
            ceaseTimer += (float)gameTime.ElapsedGameTime.Milliseconds;                
            //simulatePlayerDeath = false;
            #endregion
            #region Explosion Update
            foreach (Explosion explosion in explosions)
            {
                explosion.Update(gameTime);
            }
            explosions = explosions.Where(x => x.IsAlive).ToList();
            #endregion
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Bean bean in beans)
            {
                bean.Draw(spriteBatch);
            }

            foreach (Cease cease in ceases)
            {
                cease.Draw(spriteBatch);
            }

            foreach (Explosion explosion in explosions)
            {
                explosion.Draw(spriteBatch);
            }
        }
    }
}
