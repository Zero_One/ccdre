﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCDRE
{
    class Bean : Entity
    {
        private int value = 100;

        public Bean(Viewport viewport, Texture2D texture, Random rand)
        {
            this.viewport = viewport;
            this.texture = texture;
            this.rand = rand;
            position = new Vector2(rand.Next(viewport.Width), 0 - texture.Height);
            origin = new Vector2(texture.Width / 2, texture.Height / 2);
            moveSpeed = 2.0f;
            rotRate = .05f;
        }

        public override void Update(GameTime gameTime)
        {
            if (isAlive)
            {
                position.Y += moveSpeed;
                rotation += rotRate;
                rotation = rotation % circle;
                collisionRect = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
                if (position.Y > viewport.Height + texture.Height)
                {
                    isAlive = false;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (isAlive)
            {
                spriteBatch.Draw(texture, position, null, Color.White, rotation, origin, 1f, SpriteEffects.None, 0f);
            }
        }
    }
}
